import React from "react"
import { GatsbyImage, getImage } from "gatsby-plugin-image"

const getCover = (solution) => {
    if (solution.covers.length > 0) {
        return (<GatsbyImage className="solution-preview-image" image={getImage(solution.covers[0].img)} alt={solution.covers[0].description}/>)
    } else {
        return (<span></span>)
    }
}
const getTitle = (solution) => {
    if(solution.title) return solution.title
    else return solution.group + ":" + solution.name
}
const getDescription = (solution) => {
    if(solution.description) return (<div>{solution.description}</div>)
    else return ""
}
const getDOI = (solution) => {
    if (solution.doi) {
        console.log(solution)
        return (<><div className="solution-doi">DOI: {solution.doi != null && solution.doi != "" ? solution.doi : ""}</div></>)
    } else {
        return ("")
    }
}
const getLicense = (solution) => {
    if (solution.license) {
        return (<><div className="solution-license">License: {solution.license}</div></>)
    } else {
        return ("")
    }
}
const getCitations = (solution) => {
    if (solution.solution_citations && solution.solution_citations.length > 0) {
        return (<>{solution.solution_citations.map(citation => getCitation(citation))}</>)
    } else {
        return ("")
    }
}

const getCitation = (citation) => {
    if (citation.url) {
        return (<React.Fragment key={citation.text}><div className="solution-citation"><a href={citation.url}>{citation.text}</a></div></React.Fragment>)
    } else {
        return (<React.Fragment key={citation.text}><div className="solution-citation">{citation.text}</div></React.Fragment>)
    }
}

const SolutionPreview = ({ solution }) => {
    return (
    <div className="solution-preview">
      {getCover(solution)}
      <div className="solution-preview-text">
          <div className="solution-title">{getTitle(solution)}</div>
          {getDOI(solution)}
          <div className="solution-authors">Solution written by {solution.solution_authors.map(author => (<React.Fragment key={author.name}><span>{author.name}</span></React.Fragment>))}</div>
          {getCitations(solution)}
          {getLicense(solution)}
          <div className="solution-description">{getDescription(solution)}</div>
       </div>
       <div className="solution-preview-guide">
       <code>album install {solution.group}:{solution.name}:{solution.version}</code>
       <code>album run {solution.group}:{solution.name}:{solution.version}</code>
       </div>
    </div>
)}
export default SolutionPreview