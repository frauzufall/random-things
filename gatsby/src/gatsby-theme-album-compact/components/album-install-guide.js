import * as React from "react"
import { StaticImage } from "gatsby-plugin-image"

export default function AlbumInstallGuide({catalogUrl}) {
  return (
    <>
      <div className="install-guide">
          <h3>Album not yet installed?</h3>
          <ol>
            <li><span>Install Conda:</span><span><a href="https://docs.conda.io/projects/conda/en/latest/user-guide/install/index.html">Official Miniconda installation guide</a></span></li>
            <li><span>(Optional) Install Mamba:</span><span><code>conda install -c conda-forge mamba</code></span></li>
            <li><span>Create the Album environment:</span><span><code>conda env create -n album album</code></span></li>
          </ol>
      </div>
      <div className="install-guide">
          <h3>Before running Album..</h3>
          <ol>
            <li><span>Activate the Album environment:</span><span><code>conda activate album</code></span></li>
          </ol>
      </div>
      <div className="install-guide">
          <h3>Catalog not yet added?</h3>
          <ol>
            <li><span>Add this catalog:</span><span><code>album add-catalog {catalogUrl}</code></span></li>
          </ol>
      </div>
    </>
  );
}