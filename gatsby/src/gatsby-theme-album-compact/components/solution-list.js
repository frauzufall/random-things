import React, { useState } from "react"
import { Link } from "gatsby"
import SolutionPreview from "./solution-preview"
const SolutionList = ({ solutions, tags }) => {
    const [filter, setFilter] = useState(new Set())
    return (
        <>
        <h3>Content of this catalog</h3>
        <ul className="solution-list">
          {solutions.map(solution => {
            var hidden = false
            filter.forEach(tag => {
                if(!solution.tags.includes(tag)) hidden = true
            })
            if(hidden) return
              return (<li key={solution.group + ":" + solution.name}>
                    <SolutionPreview solution={solution}/>
                </li>)
          })}
        </ul>
        </>
    )}
export default SolutionList