module.exports = {
  pathPrefix: `/random-things/`,
  siteMetadata: {
    title: '^&~',
    subtitle: 'random things',
    catalog_url: 'https://gitlab.com/frauzufall/random-things',
    menuLinks:[
      {
         name:'About',
         link:'/about'
      }
    ]
  },
  plugins: [{ resolve: `gatsby-theme-album-compact`, options: {} }],
}
