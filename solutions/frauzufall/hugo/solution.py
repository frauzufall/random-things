from album.runner.api import setup

# Please import additional modules at the beginning of your method declarations.
# More information: https://docs.album.solutions/en/latest/solution-development/


def install():
    import subprocess
    import shutil
    from album.runner.api import get_app_path, get_package_path

    get_app_path().mkdir(exist_ok=True, parents=True)

    shutil.copy(get_package_path().joinpath('pom.xml'), get_app_path())
    shutil.copytree(get_package_path().joinpath('src'), get_app_path().joinpath('src'))

    # compile app
    subprocess.run([shutil.which('mvn'), 'compile', '-B'], cwd=get_app_path())


def run():
    import shutil
    import subprocess
    from album.runner.api import get_app_path

    subprocess.run('%s exec:java -q -Dexec.mainClass="Ghost"' % shutil.which('mvn'), shell=True, cwd=get_app_path())


setup(
    group="frauzufall",
    name="hugo",
    version="0.1.0-SNAPSHOT",
    title="Hugo the Ghost",
    description="He is a ghost and he is shy.",
    authors=["Deborah Schmidt"],
    tags=["avatar", "ghost"],
    covers=[{
        "description": "Dummy cover image.",
        "source": "cover.png"
    }],
    album_api_version="0.4.1",
    install=install,
    run=run,
    dependencies={'parent': {'resolve_solution': 'album:template-maven-java11:0.1.0'}}
)
