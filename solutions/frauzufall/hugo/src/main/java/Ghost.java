import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.scene.robot.Robot;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.util.Timer;
import java.util.TimerTask;

public class Ghost extends Application {

    private final int sideMove = 100;
    private int sideMoveCurrent = 50;
    private boolean moveLeft = true;
    private double posX, posY;
    private Robot robot;
    private ImageView ghostImage;
    private VBox ghostBox;
    private VBox mainBox;
    private Stage stage;
    private Stage primaryStage;
    private Scene scene;

    @Override
    public void start(Stage primaryStage) {

        robot = new Robot();

        this.primaryStage = primaryStage;
        stage = createStage(primaryStage);
        mainBox = new VBox();
        ghostBox = new VBox();
        ghostImage = new ImageView(new Image(getClass().getResource("/ghost.gif").toString()));
        ghostBox.getChildren().add(ghostImage);
        mainBox.getChildren().add(ghostBox);
        mainBox.setAlignment(Pos.BOTTOM_CENTER);
        ScrollPane sp = new ScrollPane(mainBox);
        mainBox.setStyle("-fx-background: transparent; -fx-background-color: transparent; ");
        sp.setStyle("-fx-background: transparent; -fx-background-color: transparent; ");
        scene = createScene(mainBox);
        stage.setScene(scene);
        primaryStage.show();
        stage.show();
        center(stage);
        whiggle(stage);
        avoidMouse(stage);
    }

    private Stage createStage(Stage primaryStage) {
        primaryStage.initStyle(StageStyle.UTILITY);
        primaryStage.setOpacity(0.);
        Stage stage = new Stage();
        stage.initOwner(primaryStage);
        stage.initStyle(StageStyle.TRANSPARENT);
        stage.setAlwaysOnTop(true);
        primaryStage.sizeToScene();
        primaryStage.setResizable(false);
        return stage;
    }

    private Scene createScene(VBox box) {
        final Scene scene = new Scene(box);
        scene.getStylesheets().add(getClass().getResource("/style.css").toExternalForm());
        scene.setFill(null);
        return scene;
    }

    private void center(Stage stage) {
        Rectangle2D primScreenBounds = Screen.getPrimary().getVisualBounds();
        double posX = (primScreenBounds.getWidth() - stage.getWidth()) / 2;
        double posY = (primScreenBounds.getHeight() - stage.getHeight()) / 2;
        updateTarget(posX, posY);
        stage.setX(this.posX);
        stage.setY(this.posY);
    }

    private void updateTarget(double posX, double posY) {
        this.posX = posX;
        this.posY = posY;
    }

    private void whiggle(Stage stage) {
        new Timer("move").scheduleAtFixedRate(new TimerTask() {
            public void run() {
                Platform.runLater(() -> {
                    if(sideMoveCurrent < 0) moveLeft = false;
                    if(sideMoveCurrent > sideMove) moveLeft = true;
                    sideMoveCurrent += moveLeft? -1 : 1;
                    stage.setX(stage.getX() + (moveLeft? -1 : 1));
                    double val = (System.nanoTime() % 200) / 200. * 2*Math.PI;
                    stage.setY(stage.getY() + (int)(Math.sin(val) * 2));
                });
            }
        }, 0, 200);
    }

    private void avoidMouse(Stage stage) {
        new Timer("avoid-mouse").scheduleAtFixedRate(new TimerTask() {
            public void run() {
                Platform.runLater(() -> {
                    int mouseX = (int) robot.getMouseX();
                    int mouseY = (int) robot.getMouseY();
                    int stageX = (int) stage.getX();
                    int stageY = (int) stage.getY();
                    double ghostRadiusWidth = ghostImage.getLayoutBounds().getWidth() / 2;
                    double ghostRadiusHeight = ghostImage.getLayoutBounds().getHeight() / 2;
                    int ghostX = (int) ghostBox.getLayoutX() + (int) ghostRadiusWidth;
                    int ghostY = (int) ghostBox.getLayoutY() + (int) ghostRadiusHeight;
                    int difX = stageX + ghostX - mouseX;
                    int difY = stageY + ghostY - mouseY;
                    double abs = Math.sqrt(difX*difX + difY*difY);
                    float scale = 10.f;
                    if(Math.abs(difX) < ghostRadiusWidth && Math.abs(difY) < ghostRadiusHeight) {
                        stage.setX(stageX + difX* (1./abs)*scale);
                        stage.setY(stageY + difY* (1./abs)*scale);
                    }
                });
            }
        }, 0, 50);
    }

    public static void main(String... args) {
        launch(args);
    }
}
