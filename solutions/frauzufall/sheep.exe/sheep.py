#
# Original open source web implementation: Adriano Petrucci (http://esheep.petrucci.ch)
# Python adaptation: Deborah Schmidt (http://esheep.petrucci.ch)
#

import math
import os
import random
import uuid
import xml.etree.ElementTree as ET
from sys import platform
from threading import Timer

from PyQt5 import QtTest
from PyQt5.QtCore import QByteArray, Qt, QEvent, QTimer
from PyQt5.QtGui import QPixmap, QImage, QTransform
from PyQt5.QtWidgets import QApplication, QLabel, QMainWindow

ACTIVATE_DEBUG = False  # show log on console


class Geometry:
    def __init__(self, name, geometry):
        self.name = name
        self.x = geometry.x
        self.y = geometry.y
        self.width = geometry.width
        self.height = geometry.height


class MainWindow(QMainWindow):

    def __init__(self, sheep, app=None, parent=None):
        super(MainWindow, self).__init__(parent)
        self.app = app
        self.sheep = sheep
        self.setWindowFlags(Qt.FramelessWindowHint | Qt.WindowStaysOnTopHint)
        self.setAttribute(Qt.WA_TranslucentBackground)
        self.installEventFilter(self)

    def eventFilter(self, source, event):
        if event.type() == QEvent.MouseButtonRelease:
            if self.sheep.dragging:
                self.sheep.dragging = False

        if event.type() == QEvent.MouseMove:
            pos = event.pos()
            if not self.sheep.dragging:
                self.sheep.dragging = True
                self.sheep.obstacle_window = None
                childsRoot = self.sheep.get(self.sheep.xml_doc, 'animations')[0]
                childs = self.sheep.get(childsRoot, 'animation')
                for k in range(len(childs)):
                    if self.sheep.get(childs[k], 'name')[0].text == "drag":
                        self.sheep.animationId = childs[k].attrib["id"]
                        self.sheep.animation_step = 0
                        self.sheep.animation_node = childs[k]
                        break
            if self.sheep.dragging:
                self.sheep.image_x = int(int(pos.x() + self.pos().x()) - self.sheep.image_w / 2)
                self.sheep.image_y = int(int(pos.y() + self.pos().y()) - self.sheep.image_h / 2)
                self.sheep.widget.move(self.sheep.image_x, self.sheep.image_y)
        return QMainWindow.eventFilter(self.app, source, event)


class Sheep:
    def __init__(self, app=None, is_child=False):

        self.animationFile = None
        self.id = uuid.uuid4()
        if app:
            self.app = app
        else:
            self.app = QApplication([])

        self.m = MainWindow(self, self.app)

        self.timer = QTimer()
        self.timer.timeout.connect(self._next_sheep_step)
        desktop_geometry = self._get_desktop_geometry()

        self.widget = self.m  # Div added to webpage, containing the sheep
        self.img_label = QLabel(self.m)  # Tile image, will be positioned inside the div

        self.xml_doc = None  # parsed XML Document
        self.prepare_to_die = False  # when removed, animations should be stopped

        self.is_child = is_child  # Child will be removed once they reached the end

        self.tilesX = 1  # Quantity of images inside Tile
        self.tilesY = 1  # Quantity of images inside Tile
        self.image_w = 1  # Width of the sprite image
        self.image_h = 1  # Height of the sprite image
        self.image_x = 1  # Position of sprite inside webpage
        self.image_y = 1  # Position of sprite inside webpage
        self.flipped = False  # if sprite is flipped
        self.dragging = False  # if user is dragging the sheep
        self.animationId = 0  # current animation ID
        self.animation_step = 0  # current animation step
        self.animation_node = None  # current animation DOM node
        self.obstacle_window = None  # the HTML element where the pet is walking on
        self.randS = random.random() * 100  # random value, will change when page is reloaded

        self.ewmh_instance = None

        self.screen_w = desktop_geometry.width()
        self.screen_h = desktop_geometry.height()

    def start(self, animation):
        self._parse_xml(animation)
        self.animationFile = animation
        self.m.show()
        if not self.is_child:
            self.app.exec()

    def get(self, doc, name):
        return doc.findall('./{https://esheep.petrucci.ch/}%s' % name)

    def _get_desktop_geometry(self):
        screen = self.app.primaryScreen()
        if ACTIVATE_DEBUG:
            print('Screen: %s' % screen.name())
        self.desktop_size = screen.size()
        if ACTIVATE_DEBUG:
            print('Size: %d x %d' % (self.desktop_size.width(), self.desktop_size.height()))
        self.desktop_available = screen.availableGeometry()
        if ACTIVATE_DEBUG:
            print('Available: %d x %d' % (self.desktop_available.width(), self.desktop_available.height()))
        return self.desktop_size

    def _parse_xml(self, text):
        tree = ET.parse(text)
        self.xml_doc = tree.getroot()
        doc_ = self.xml_doc
        image = self.get(doc_, 'image')[0]
        self.tilesX = int(self.get(image, "tilesx")[0].text)
        self.tilesY = int(self.get(image, "tilesy")[0].text)

        by = QByteArray.fromBase64(str.encode(self.get(image, "png")[0].text))
        qimg = QImage.fromData(by)
        self.pixmap = QPixmap(qimg)
        transform = QTransform()
        transform.rotate(180, axis=Qt.YAxis)
        self.pixmap_flipped = self.pixmap.transformed(transform)
        self.img_label.setPixmap(self.pixmap)

        if ACTIVATE_DEBUG:
            print("Sprite image loaded: %s, %s" % (self.pixmap.width(), self.pixmap.height()))
        self.img_label.resize(self.pixmap.width(), self.pixmap.height())
        self.img_label.setMinimumSize(self.pixmap.width(), self.pixmap.height())
        self.img_label.setMaximumSize(self.pixmap.width(), self.pixmap.height())
        # prevent to move image (will show the entire sprite sheet if not catched)
        self.image_w = int(self.pixmap.width() / self.tilesX)
        self.image_h = int(self.pixmap.height() / self.tilesY)
        self.widget.resize(int(self.image_w), int(self.image_h))
        self.widget.setMaximumSize(int(self.image_w), int(self.image_h))
        self.widget.setMinimumSize(int(self.image_w), int(self.image_h))
        self.widget.move(int(self.image_x), int(self.image_y))

        if self.is_child:
            self._spawn_child()
        else:
            self._spawn_sheep()

    def _set_position(self, x, y, absolute):
        if self.widget:
            if absolute:
                self.image_x = int(x)
                self.image_y = int(y)
            else:
                self.image_x = int(self.image_x) + int(x)
                self.image_y = int(self.image_y) + int(y)
            # if ACTIVATE_DEBUG:
            #     print("Moving to %s, %s" % (self.image_x, self.image_y))
            self.widget.move(self.image_x, self.image_y)

    def _spawn_sheep(self):
        if ACTIVATE_DEBUG:
            print("Spawn sheep")
        spawnsRoot = self.get(self.xml_doc, 'spawns')[0]
        spawns = self.get(spawnsRoot, 'spawn')
        prob = 0
        for i in range(len(spawns)):
            prob += int(spawns[0].attrib["probability"])
        rand = random.random() * prob
        prob = 0
        for i in range(len(spawns)):
            prob += int(spawns[i].attrib["probability"])
            if prob >= rand or i == len(spawns) - 1:
                self._set_position(
                    self._parse_keywords(self.get(spawns[i], 'x')[0].text),
                    self._parse_keywords(self.get(spawns[i], 'y')[0].text),
                    True
                )
                if ACTIVATE_DEBUG:
                    print("Spawn: %s, %s" % (self.image_x, self.image_y))
                self.animationId = int(self.get(spawns[i], 'next')[0].text)
                self.animation_step = 0
                childsRoot = self.get(self.xml_doc, 'animations')[0]
                childs = self.get(childsRoot, 'animation')
                for k in range(len(childs)):
                    if int(childs[k].attrib["id"]) == self.animationId:
                        self.animation_node = childs[k]

                        # Check if child should be loaded toghether with this animation
                        childsRoot = self.get(self.xml_doc, 'childs')[0]
                        childs = self.get(childsRoot, 'child')
                        for j in range(len(childs)):
                            if int(childs[j].attrib["animationid"]) == self.animationId:
                                if ACTIVATE_DEBUG:
                                    print("Child from Spawn")
                                eSheepChild = Sheep(self.app, True)
                                eSheepChild.animationId = int(self.get(childs[j], 'next')[0].text)
                                x = self.get(childs[j], 'x')[0]
                                y = self.get(childs[j], 'y')[0]
                                eSheepChild._set_position(self._parse_keywords(x), self._parse_keywords(y), True)
                                # Start animation
                                eSheepChild.start(self.animationFile)
                                break
                        break
                break
            # Play next step
        self._next_sheep_step()

    def _spawn_child(self):
        if ACTIVATE_DEBUG:
            print("Spawn child")
        childsRoot = self.get(self.xml_doc, 'animations')[0]
        childs = self.get(childsRoot, 'animation')
        for k in range(len(childs)):
            child_id = int(childs[k].attrib["id"])
            if child_id == self.animationId:
                self.animation_node = childs[k]
                break
        self._next_sheep_step()

        # Parse the human readable expression from XML to a computer readable expression

    def _parse_keywords(self, value):
        value = value.replace('screenW', str(self.screen_w))
        value = value.replace('screenH', str(self.screen_h))
        value = value.replace('areaW', str(self.screen_h))
        value = value.replace('areaH', str(self.screen_h))
        value = value.replace('imageW', str(self.image_w))
        value = value.replace('imageH', str(self.image_h))
        value = value.replace('random', str(random.random() * 100))
        value = value.replace('randS', str(self.randS))
        value = value.replace('imageX', str(self.image_x))
        value = value.replace('imageY', str(self.image_y))

        ret = 0
        try:
            ret = eval(value)
        except Exception as e:
            print("Unable to parse this position: \n'" + value + "'\n Error message: \n" + str(e))
        return ret

    def _get_next_random_node(self, parentNode):
        baseNode = self.get(parentNode, 'next')
        childsRoot = self.get(self.xml_doc, 'animations')[0]
        childs = self.get(childsRoot, 'animation')
        prob = 0
        nodeFound = False

        # no more animations (it was the last one)
        if len(baseNode) == 0:
            # If it is a child, remove the child from document
            if self.is_child:
                # remove child
                if ACTIVATE_DEBUG:
                    print("Remove child")
                self.widget.close()
                # else, spawn sheep again
            else:
                self._spawn_sheep()
            return False

        for k in range(len(baseNode)):
            prob += int(baseNode[k].attrib["probability"])
        rand = random.random() * prob
        index = 0
        prob = 0
        for k in range(len(baseNode)):
            prob += int(baseNode[k].attrib["probability"])
            if prob >= rand:
                index = k
                break
        for k in range(len(childs)):
            if childs[k].attrib["id"] == baseNode[index].text:
                self.animationId = int(childs[k].attrib["id"])
                self.animation_step = 0
                self.animation_node = childs[k]
                nodeFound = True
                break

        if nodeFound:  # create Child, if present
            childsRoot = self.get(self.xml_doc, 'childs')[0]
            childs = self.get(childsRoot, 'child')
            for k in range(len(childs)):
                if int(childs[k].attrib["animationid"]) == self.animationId:
                    if ACTIVATE_DEBUG:
                        print("Child from Animation")
                    eSheepChild = Sheep(self.app, True)
                    eSheepChild.animationId = int(self.get(childs[k], 'next')[0].text)
                    x = self.get(childs[k], 'x')[0].text
                    y = self.get(childs[k], 'y')[0].text
                    eSheepChild._set_position(self._parse_keywords(x), self._parse_keywords(y), True)
                    eSheepChild.start(self.animationFile)
                    break

        return nodeFound

    def _check_overlapping(self):
        x = self.image_x
        y = self.image_y + self.image_h
        margin = 20
        if self.obstacle_window:
            margin = 5

        els = self._get_windows()

        for i in range(len(els)):
            geometry = els[i]
            if geometry.y - 2 < y < geometry.y + margin:
                if geometry.x < x < geometry.x+geometry.width - self.image_w:
                    return geometry
        return False

    def _get_windows(self):
        els = []
        if platform == "linux" or platform == "linux2":
            from ewmh import EWMH
            if not self.ewmh_instance:
                self.ewmh_instance = EWMH()
            try:
                els = [self._get_geometry(win) for win in self.ewmh_instance.getClientList()
                       if '_NET_WM_STATE_HIDDEN' not in self.ewmh_instance.getWmState(win, str=True)]
            except Exception:
                # this sometimes fails but we don't want the sheep to freeze just because of not being able to locate windows
                pass
        elif platform == "darwin":
            # TODO
            pass
        elif platform == "win32":
            # TODO
            pass
        return els

    def _get_geometry(self, window):
        while window.query_tree().parent != self.ewmh_instance.root:
            window = window.query_tree().parent
        geometry = Geometry(window.get_wm_name(), window.get_geometry())
        # geometry.x += self.desktop_available.x()
        geometry.y += self.desktop_available.y()
        return geometry

    def _get_node_value(self, nodeName, valueName, defaultValue):
        if not self.animation_node:
            return
        animationSubnode = self.get(self.animation_node, nodeName)
        if not animationSubnode:
            return
        if self.get(animationSubnode[0], valueName):
            value = self.get(animationSubnode[0], valueName)[0].text
            return self._parse_keywords(value)
        else:
            return defaultValue

    def _next_sheep_step(self):
        self.timer.stop()
        if self.prepare_to_die:
            return

        # if ACTIVATE_DEBUG:
        #     print('animation step %s' % self.animation_step)

        x1 = self._get_node_value('start', 'x', 0)
        y1 = self._get_node_value('start', 'y', 0)
        off1 = self._get_node_value('start', 'offsety', 0)
        opa1 = self._get_node_value('start', 'opacity', 1)
        del1 = self._get_node_value('start', 'interval', 1000)
        x2 = self._get_node_value('end', 'x', 0)
        y2 = self._get_node_value('end', 'y', 0)
        off2 = self._get_node_value('end', 'offsety', 0)
        opa2 = self._get_node_value('end', 'interval', 1)
        del2 = self._get_node_value('end', 'interval', 1000)

        sequence = self.get(self.animation_node, 'sequence')[0]
        repeat = self._parse_keywords(sequence.attrib['repeat'])
        repeatfrom = int(sequence.attrib['repeatfrom'])
        gravity = self.get(self.animation_node, 'gravity')
        border = self.get(self.animation_node, 'border')

        frames = self.get(sequence, 'frame')
        steps = len(frames) + (len(frames) - repeatfrom) * repeat

        if self.animation_step < len(frames):
            index = frames[self.animation_step].text
        elif repeatfrom == 0:
            index = frames[self.animation_step % len(frames)].text
        else:
            index = frames[int(repeatfrom) + int((self.animation_step - repeatfrom) % (len(frames) - repeatfrom))].text

        index = int(index)
        # if ACTIVATE_DEBUG:
        #     print("Displaying index %s" % index)

        x = - self.image_w * (index % self.tilesX)
        y = - self.image_h * int(index / self.tilesX)
        if self.flipped:
            x = -(self.pixmap.width() - self.image_w + x)
            self.img_label.move(x, y)
        else:
            self.img_label.move(x, y)

        if self.dragging:
            self.animation_step += 1
            self.timer.start(50)
            return

        if self.flipped:
            x1 = -x1
            x2 = -x2

        if self.animation_step == 0:
            self._set_position(x1, y1, False)
        else:
            self._set_position(int(x1) + int((x2 - x1) * self.animation_step / steps),
                               int(y1) + int((y2 - y1) * self.animation_step / steps),
                               False)

        self.animation_step += 1

        if self.animation_step >= steps:
            actions = self.get(sequence, 'action')
            if len(actions) > 0:
                action = actions[0].text
                if action == "flip":
                    if not self.flipped:
                        self.img_label.setPixmap(self.pixmap_flipped)
                        self.flipped = True
                    else:
                        self.img_label.setPixmap(self.pixmap)
                        self.flipped = False
            if not self._get_next_random_node(self.get(self.animation_node, 'sequence')[0]):
                return

        set_next = False

        if border and border[0] and self.get(border[0], 'next'):
            if x2 < 0 and self.image_x < 0:
                self.image_x = 0
                set_next = True
            elif x2 > 0 and self.image_x > self.screen_w - self.image_w:
                self.image_x = self.screen_w - self.image_w
                self.widget.move(int(self.image_x), self.widget.y())
                set_next = True
            elif y2 < 0 and self.image_y < 0:
                self.image_y = 0
                set_next = True
            elif y2 > 0 and self.image_y > self.screen_h - self.image_h:
                self.image_y = self.screen_h - self.image_h
                set_next = True
            elif y2 > 0:
                if self._check_overlapping():
                    if self.image_y > self.image_h:
                        self.obstacle_window = self._check_overlapping()
                        self.image_y = math.ceil(self.obstacle_window.y) - self.image_h
                        set_next = True
            elif self.obstacle_window:
                if not self._check_overlapping():
                    if (self.image_y + self.image_h > self.obstacle_window.y + 3 or
                            self.image_y + self.image_h < self.obstacle_window.y - 3):
                        self.obstacle_window = None
                    elif self.image_x < self.obstacle_window.x:
                        self.image_x = int(self.image_x + 3)
                        set_next = True
                    else:
                        self.image_x = int(self.image_x - 3)
                        set_next = True
                    self.widget.move(int(self.image_x), self.widget.y())
            if set_next:
                if not self._get_next_random_node(border[0]):
                    return
        if not set_next and gravity and gravity[0] and self.get(gravity[0], 'next'):
            if self.image_y < self.screen_h - self.image_h - 2:
                if not self.obstacle_window:
                    set_next = True
                else:
                    if not self._check_overlapping():
                        set_next = True
                        self.obstacle_window = None

                if set_next:
                    if not self._get_next_random_node(gravity[0]):
                        return
        if not set_next:
            if (self.image_x < - self.image_w and x2 < 0) or (self.image_x > self.screen_w and x2 > 0) or (
                    self.image_y < - self.image_h and y1 < 0) or (self.image_y > self.screen_h and y2 > 0):
                set_next = True
                if not self.is_child:
                    self._spawn_sheep()
                return

        self.timer.start(int(float(int(del1) + int((del2 - del1) * self.animation_step / steps))))


if __name__ == '__main__':
    app = QApplication([])
    sheep = Sheep(app)
    sheep.start('%s/%s' % (os.path.dirname(os.path.realpath(__file__)), "animation.xml"))