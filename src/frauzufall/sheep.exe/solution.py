from album.runner.api import setup

# Please import additional modules at the beginning of your method declarations.
# More information: https://docs.album.solutions/en/latest/solution-development/


def run():
    import sys
    from album.runner.api import get_package_path
    sys.path.insert(0, str(get_package_path()))
    from sheep import Sheep
    sheep = Sheep()
    sheep.start(get_package_path().joinpath("animation.xml"))


env_file = """channels:
- default
dependencies:
- python=3.9
- pip=21.2.4
- pyqt=5.9.2
- pip:
  - ewmh>=0.1.6
"""


setup(
    group='frauzufall',
    name='sheep.exe',
    version='0.1.0-SNAPSHOT',
    title='Sheep.exe aka eSheep',
    description='Python adaptation of the popular desktop pet from the 90\'s.',
    authors=['Deborah Schmidt'],
    cite=[{
            'text': 'Tatsutoshi Nomura, the original author of Stray Sheep',
        }, {
            'text': 'Adriano Petrucci, author of the Open Source version of eSheep.',
            'url': 'https://github.com/Adrianotiger/desktopPet'
    }],
    tags=['avatar', 'sheep.exe'],
    covers=[{
        'description': 'That\'s the sheep on top of a terminal window.',
        'source': 'cover.png'
    }],
    album_api_version='0.4.1',
    run=run,
    dependencies={'environment_file': env_file}
)
