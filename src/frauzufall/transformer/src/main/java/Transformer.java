import javafx.animation.AnimationTimer;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;

public class Transformer {

	private final Stage window;
	private final Button transformButton;

	public Transformer() {
		window = new Stage();
		transformButton = new Button("TRANSFORM");
		transformButton.setOnAction(this::transform);
		VBox box = new VBox(transformButton);
		box.setAlignment(Pos.CENTER);
		VBox.setVgrow(box, Priority.ALWAYS);
		Scene scene = new Scene(box);
		window.setAlwaysOnTop(true);
		window.setScene(scene);
		window.setWidth(800);
		window.setHeight(500);
		window.show();
	}

	private void transform(ActionEvent event) {
		transformButton.setVisible(false);
		Rectangle2D bounds = Screen.getPrimary().getVisualBounds();
		double factor = 3;
		AnimationTimer t = new AnimationTimer() {
			private long lastUpdate = 0;
			@Override
			public void handle(long now) {

				if (now - lastUpdate >= 20_000_000) {
					double newWidth = Math.max(80, window.getWidth() - factor);
					double newHeight = Math.max(80, window.getHeight() - factor/2);
					window.setWidth(newWidth);
					window.setHeight(newHeight);
					window.setY(window.getY()+factor);
					if( window.getY()+newHeight >= bounds.getMaxY()){
						this.stop();
						window.setResizable(false);
						addWheels(window);
					}
					lastUpdate = now;
				}
			}
		};
		t.start();
	}

	private void addWheels(Stage window) {
		AnimationTimer t = new AnimationTimer() {

			private long lastUpdate = 0;
			private boolean initialized = false;
			private Window windowWheel1;
			private Window windowWheel2;
			private double goalSize;
			private double factor;
			private Rectangle2D bounds;
			private Stage wheel1;
			private Stage wheel2;
			private ImageView imageView1;
			private ImageView imageView2;

			private void init() {
				Image wheelImage = new Image(getClass().getResourceAsStream("wheel.png"));
				imageView1 = new ImageView(wheelImage);
				imageView2 = new ImageView(wheelImage);
				wheel1 = addWheel(imageView1);
				wheel2 = addWheel(imageView2);
				bounds = Screen.getPrimary().getVisualBounds();

				wheel1.show();
				wheel2.show();

				windowWheel1 = wheel1.getScene().getWindow();
				windowWheel2 = wheel2.getScene().getWindow();

				windowWheel1.setWidth(0);
				windowWheel1.setHeight(0);
				windowWheel2.setWidth(0);
				windowWheel2.setHeight(0);
				goalSize = 40;
				factor = 2;
			}

			@Override
			public void handle(long now) {

				if(!initialized) {
					init();
					initialized = true;
				}

				if (now - lastUpdate >= 20_000_000) {
					double newSize = windowWheel1.getWidth() + factor;
					double wheelY = bounds.getMaxY() - newSize;
					double windowY = bounds.getMaxY() - newSize / 2 - window.getHeight();
					windowWheel1.setWidth(newSize);
					windowWheel1.setHeight(newSize);
					windowWheel2.setWidth(newSize);
					windowWheel2.setHeight(newSize);
					windowWheel1.setY(wheelY);
					windowWheel2.setY(wheelY);
					windowWheel1.setX(window.getX() + goalSize - newSize / 2);
					windowWheel2.setX(window.getX() + window.getWidth() - goalSize - newSize / 2);
					window.setY(windowY);
					if (newSize >= goalSize) {
						this.stop();
						window.setResizable(false);
						moveAway(window, wheel1, wheel2, imageView1, imageView2);
					}
					lastUpdate = now;
				}
			}
		};
		t.start();

	}

	private void moveAway(Stage window, Stage wheel1, Stage wheel2, ImageView wheel1Image, ImageView wheel2Image) {
		window.setOpacity(1.0);
		Image wheelImage = new Image(getClass().getResourceAsStream("wheel.gif"));
		wheel1Image.setImage(wheelImage);
		wheel2Image.setImage(wheelImage);
		AnimationTimer t = new AnimationTimer() {
			private long lastUpdate = 0;
			Stage _window = window;
			Stage _wheel1 = wheel1;
			Stage _wheel2 = wheel2;
			Rectangle2D bounds = Screen.getPrimary().getVisualBounds();
			private double last_x = _window.getX();
			float factor = 3;
			@Override
			public void handle(long now) {
				if (now - lastUpdate >= 50_000_000) {
					double new_x = Math.max(bounds.getMinX(), last_x - factor);
					double width = _window.getWidth();
					last_x = new_x;
					_window.setX(new_x);
					_wheel1.setX(new_x+_wheel1.getWidth()*0.5);
					_wheel2.setX(new_x+width-_wheel1.getWidth()*1.5);
					if(new_x == bounds.getMinX()) {
						this.stop();
						moveOut(_window, _wheel1, _wheel2);
					}
					lastUpdate = now;
				}
			}
		};
		t.start();
	}

	private void moveOut(Stage window, Stage wheel1, Stage wheel2) {
		window.setX(0);
		AnimationTimer t = new AnimationTimer() {
			private long lastUpdate = 0;
			Stage _window = window;
			Stage _wheel1 = wheel1;
			Stage _wheel2 = wheel2;
			Rectangle2D bounds = Screen.getPrimary().getVisualBounds();
			private double last_w = _window.getWidth();
			float factor = 3;
			@Override
			public void handle(long now) {
				if (now - lastUpdate >= 50_000_000) {
					double new_width = Math.max(0, last_w-factor);
					last_w = new_width;
					_window.setWidth(new_width);
					_wheel2.setX(bounds.getMinX() + new_width-_wheel1.getWidth()*1.5);
					double alpha = Math.max(0.0, Math.min(1.0, (last_w - 100) / 100.0));
					_window.setOpacity(alpha);
					_wheel1.setOpacity(Math.max(0, _wheel1.getOpacity() - 0.3));
					_wheel2.setOpacity(alpha);
					if( _window.getWidth() == 0){
						_window.close();
						_wheel1.close();
						_wheel2.close();
						this.stop();
					}
					lastUpdate = now;
				}
			}
		};
		t.start();
	}

	private Stage addWheel(ImageView imageView) {
		imageView.setPreserveRatio(true);
		VBox wheel = new VBox(imageView);
		wheel.setStyle("-fx-background: transparent; -fx-background-color: transparent; ");
		Stage dialog = new Stage();
		dialog.initOwner(window);
		dialog.initStyle(StageStyle.TRANSPARENT);
		dialog.setAlwaysOnTop(true);
		Scene dialogPane = new Scene(wheel);
		dialogPane.setFill(null);
		wheel.setPadding(Insets.EMPTY);
		wheel.setBackground(Background.EMPTY);
		dialog.setScene(dialogPane);
		imageView.fitWidthProperty().bind(dialog.widthProperty());
		imageView.opacityProperty().bind(dialog.opacityProperty());
		return dialog;
	}


	public static void main(String... args) {
		Platform.startup(() -> {});
		Platform.runLater(() -> new Transformer());
	}
}
